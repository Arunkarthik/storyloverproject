import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myForm: FormGroup= new FormGroup({}); 
  public loading: boolean = false;
  constructor(public fb: FormBuilder
    ,private authservice:UserService
    ,private snack:MatSnackBar
    ,private route :Router) { }

  ngOnInit(): void {
    this.reactiveForm(); 
  }
  reactiveForm() {
    this.myForm = this.fb.group({
      Username: new FormControl('',[Validators.required]),
      EmailId: new FormControl('',[Validators.required,Validators.email]),
      Password: new FormControl('',[Validators.required]),
  
    })
  }
  submitForm() {
    console.log(this.myForm.value)
    if(!this.myForm.valid){
      return;
    }
  }
  post:any;
  SignupPost(){
    this.loading = true;
    this.authservice.postUserData(this.myForm.value).subscribe((response:any)=>{
     this.post=response;
     if(response){
    
       this.snack.open("Registered Great","ok");
      
       this.loading = false;
     }
     else{
     
     this.snack.open("OOPS");}
     this.loading = false;
    })
  }

  public errorHandling = (control: string, error: string) => {
    return this.myForm.controls[control].hasError(error);
  }
  

}
