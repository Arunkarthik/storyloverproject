import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
  myForm: FormGroup= new FormGroup({}); 
  public loading: boolean = false;
  constructor(private fb: FormBuilder
    ,private authservice:UserService
    ,private authsnack:MatSnackBar
    ,private route:Router) { }

  ngOnInit(): void {
    this.reactiveForm()
  }
  reactiveForm() {
    this.myForm = this.fb.group({
    
      EmailId: new FormControl('',[Validators.required]),
      Password: new FormControl('',[Validators.required]),
  
    })
  }
  signinform() {
    this.loading = true;
    console.log(this.myForm.value)
    this.authservice.getLogin(this.myForm.value).subscribe((response:any)=>{
      if(response){
        this.authsnack.open("login worked","ok");
        localStorage.setItem('users',JSON.stringify(response.data));
        this.route.navigate(["/dash/Dashboard"]);
        this.loading = false;
      }
      else{ this.authsnack.open("wrong","ok");
      this.loading = false;
    }

    })
    
  }
  public errorHandling = (control: string, error: string) => {
    return this.myForm.controls[control].hasError(error);
  }

  
}
