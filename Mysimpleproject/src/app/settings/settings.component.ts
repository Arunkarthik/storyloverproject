import { HttpClient , HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  url = '';
  uploaded = false;
  profileInfo = new FormGroup({
    FirstName : new FormControl(''),
    LastName  : new FormControl(''),
    Dob  : new FormControl(''),
    Email : new FormControl(''),
    MobileNumber : new FormControl(''),
    Country : new FormControl(''),
    State : new FormControl(''),
    City : new FormControl(''),
    Address1 : new FormControl(''),
    Address2 : new FormControl(''),
    Bio : new FormControl('')
  });
  constructor(public http: HttpClient
    ,public fb:FormBuilder
    ,public myserv:UserService) { }
 
  ngOnInit(): void {
   

   
  }

 

  onSelectFile(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
         
      }
    }
  }
  public delete(){
    this.url ='';
  }

  onMouseEnter(){
     this.uploaded = false;
  }

  saveProfile(){
      console.log(this.profileInfo.value);
      this.myserv.saveProfile(this.profileInfo.value).subscribe((response:any)=>{
        if(response){
        
            Swal.fire('Hi', 'Profile Information saved', 'success');
            localStorage.setItem('users',response);
        
        }
        else
        Swal.fire('Hi', 'OOPS', 'error');
      })
  }

}
