import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  picture:any;
  constructor(public user:UserService,public router:Router) { }

  ngOnInit(): void {
  
  }
  
  Account(){
    this.router.navigateByUrl('Signup')
  }
  
}
