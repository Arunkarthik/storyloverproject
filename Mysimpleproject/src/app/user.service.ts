import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http:HttpClient) { }

  postUserData(users:any){
    return this.http.post("http://localhost:2000/user-register",users);
  }

  getLogin(validuser:any){
    return this.http.post("http://localhost:2000/user-login",validuser);
  }

  saveProfile(formdata:any){
    return this.http.post("http://localhost:8000/save-profile",formdata)
  }
}
