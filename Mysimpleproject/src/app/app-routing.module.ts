import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { LibraryComponent } from './library/library.component';
import { MystoriesComponent } from './mystories/mystories.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';
import { UserloginComponent } from './userlogin/userlogin.component';

const routes: Routes = [
  {path:'',component:HomeComponent,redirectTo:'',pathMatch:'full'},
  {path:'Signup',component:RegisterComponent},
  {path:'Signup/Signin',component:UserloginComponent},
  {path:'dash',component:NavbarComponent,
  children:[
    {path:'Dashboard',component:DashboardComponent},
    {path:'Allstories',component:LibraryComponent},
    {path:'Settings',component:SettingsComponent},
    {path:'mystories',component:MystoriesComponent},
   
  ]},
  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
